import random
import unittest

class Test1(unittest.TestCase):
	def setUp(self):
		self.seq = range(8)

	def test_shuffle(self):
		shuffled = self.seq[:]
		random.shuffle(shuffled)
		shuffled.sort()
		self.assertEqual(self.seq, shuffled)

	def test_choice(self):
		element = random.choice(self.seq)
		self.assertTrue(element in self.seq)

if __name__ == '__main__':
	unittest.main()


