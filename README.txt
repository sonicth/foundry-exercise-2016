* How did you approach solving this problem?

I have chosen a solution based on graphs, where the "intermediate" relation of words seemed like a perfect fit for 'adjacency' link between vertices, and the shortest "transformation" list - for the shortest 'path'. The solution is split into two parts. The part (1), transition graph construction, I have implemented a naive (complexity O(n^2*l) where n and l are dictionary and word lengths accordingly), and a more performant (O(n*l^2), see 'hashTransitions.py') algorithms. For the part (2) a Dijkstra algorithm from NetworkX python module was used (also I strongly believe in code reuse, especially in the context of a larger project!).


* How did you check that your solution is correct?

I have initially written a number of tests using "unittest" python module, that failed, to begin with. I have since implemented the solution and added a few more tests.

Testing is done for:
-bad input, for example, that each word is a lower-case alphabetic string, and others (please see wtUnitTests.py)
-the existence of a word in the dictionary, if a word can have any transitions at all, and finally if there is a path between 'start' and 'end'
-checking that a path or a transition list is valid, where neighbour words differ by exactly one character
-edge cases: long dictionaries with short words (one character), long words (20k) with short dictionaries.
-checking transitions of random words in a context of a subset of a real-world English dictionary.


* Specify any assumptions
An assumption that the length of dictionary and words were finite, however, given the Real-life scenario dictionary sizes would be much larger than the word sizes, which is also shown in the fact that the second transition construction algorithm performs much better.