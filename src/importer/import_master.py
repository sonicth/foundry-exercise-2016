import sys

#NOTE
# download master from http://norvig.com/npdict.txt
master_path = "../../data/npdict.txt"

# output file
output_path = "../../data/alpha_dict.txt"

if __name__ == '__main__':
	try:
		# open for reading
		f = open(master_path)

		entries = list(f)
		num_entries = len(entries)

		print "read {0} entries from {1}".format(num_entries, master_path)
		words = []

		for entry in entries:
			stripped_newlines = entry.rstrip()

			stripped_articles = ''
			if stripped_newlines.startswith('a '):
				stripped_articles = stripped_newlines[len('a '):]
			elif stripped_newlines.startswith('an '):
				stripped_articles = stripped_newlines[len('an '):]
			else:
				stripped_articles = stripped_newlines

			# proceed if only all alphabetic
			if stripped_articles.isalpha():
				# convert to lower case
				word = stripped_articles.lower()

				words.append(word)

		# write to file
		fw = open(output_path, 'w')

		for item in words:
			print >> fw, item

		num_entries_written = len(words)
		print "written {0} entries to {1}".format(num_entries_written, output_path)

		print "done."

	except IOError:
		print "check that file '{0}' exists and is readable, and {1} can be written to!".format(master_path, output_path)
	except:
		print("Unexpected error:", sys.exc_info()[0])
