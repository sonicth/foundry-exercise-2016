import networkx as nx

# naively, i.e. in n^2, create adjacency relationships for all nodes (words) and return the graph
def createGraphNaive(dictionary):
	graph = nx.Graph()

	# add connections to graph is there is an edge
	#	init indices
	w_i = 0;
	w_other_i = 0

	#	n^2 loop!!!
	for word in dictionary:
		for word_other in dictionary:
			# add to graph is transition possible
			if _isIntermediate(word, word_other):
				graph.add_edge(word, word_other)

			w_other_i += 1
		w_i += 1

	return graph

# words are intermediate (or adjacent) if they differ by exactly 1 character!
def _isIntermediate(word1, word2):
	# lengths must match!
	assert (len(word1) == len(word2))

	num_mismatched_chars = 0
	for char1, char2 in zip(word1, word2):
		if char1 != char2:
			num_mismatched_chars += 1

	# words are intermediate if they differ by exactly one character
	if num_mismatched_chars == 1:
		return True
	else:
		return False

# check correctness of the transition
def checkTransitions(words):
	num_words = len(words)
	assert(num_words > 1)

	curr_word = words[0]
	# check transition correctness
	for i in range(1, num_words):
		next_word = words[i]
		if not _isIntermediate(curr_word, next_word):
			return False
		curr_word = next_word

	return True