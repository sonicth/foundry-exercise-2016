
class ReadDictionary:
	def __init__(self, dict_path = "../data/alpha_dict.txt"):
		try:
			f = open(dict_path)
			entries = list(f)
			self.words = map(lambda entry: entry.rstrip(), entries)
		except:
			print "failed to read {0}".format(dict_path)
			self.words = []

	def wordsWithLen(self, length = 3):
		filtered_words =  filter(lambda word: len(word) == length, self.words)
		return filtered_words
