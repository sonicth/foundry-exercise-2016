import random
import unittest
import wordsTransformator as wtr
from wordsTransformator import WordsTransformator
from readDictionary import ReadDictionary
import naiveTransitions as nt

################################################################
# basic test: specified conditions
################################################################
class TestSpecification(unittest.TestCase):
	def setUp(self):
		# initialise transformer with specified dictionary
		self.wt = WordsTransformator(["hit", "dot", "dog", "cog", "hot", "log"])
		# also test with naive transformer
		self.wt_naive = WordsTransformator(["hit", "dot", "dog", "cog", "hot", "log"], True)

	def test_specification(self):
		# number of transformation should match too
		num_transforms = self.wt.findNumTransformations("hit", "cog")
		self.assertEqual(num_transforms, 4)

		# path should match
		path = self.wt.findRoute("hit", "cog")
		# check transition
		self.assertTrue(nt.checkTransitions(path))
		# check against specification
		self.assertEqual(path, ["hit", "hot", "dot", "dog", "cog"])

		# do naive test too..
		path2 = self.wt_naive.findRoute("hit", "cog")
		self.assertEqual(path2, path)



################################################################
# other tests
################################################################

# list of English words
en_dictionary_path = "../data/alpha_dict.txt"

# lower case alphabetic characters
alphabet = map(chr, range(ord('a'), ord('z')+1))

# test correctness of word dictionary
class TestBadInput(unittest.TestCase):
	# is list?
	def test_dictionary_notarray(self):
		# raise exception unless array
		self.assertRaises(wtr.TransformatorError, WordsTransformator, 1024)
		self.assertRaises(wtr.TransformatorError, WordsTransformator, {})
		# ..empty array should not raise
		WordsTransformator([])

	# correct string?
	def test_dictionary_badtype(self):
		# not strings
		self.assertRaises(wtr.BadtypeError, WordsTransformator, [1, 2, 3])
		# non-alphabetic
		self.assertRaises(wtr.BadtypeError, WordsTransformator, ["one-two", "two'six", "one1one"])
		# not lower-case
		self.assertRaises(wtr.BadtypeError, WordsTransformator, ["cat", "Dog", "rat"])

	# correct dictionary format?
	def test_dictionary_format(self):
		# unequal lengths
		self.assertRaises(wtr.BadformatError, WordsTransformator, ["cat", "trout", "elephant"])

# test correctness of *start* and *end* words
class TestNodeBadInput(unittest.TestCase):
	# start with a good transformator
	def setUp(self):
		self.transformer = WordsTransformator(["hit", "dot", "dog", "cog", "hot", "log"])

	# start and end words correctness
	def test_node_badtype(self):
		self.assertRaises(wtr.BadtypeError, self.transformer.findRoute, "hit", 2)
		self.assertRaises(wtr.BadtypeError, self.transformer.findRoute, {}, "cog")
		# non-alphabetic
		self.assertRaises(wtr.BadtypeError, self.transformer.findRoute, "aaa", "b-b")
		# not lower-case
		self.assertRaises(wtr.BadtypeError, self.transformer.findRoute, "aaA", "bbb")
		# bad lengths
		self.assertRaises(wtr.BadformatError, self.transformer.findRoute, "hiit", "aaa")
		self.assertRaises(wtr.BadformatError, self.transformer.findRoute, "bbb", "cooge")
		# this should be ok:
		# 		self.transformer.findRoute("hit", "cog")

	# empty dictionary should produce empty path
	def test_node_emptydictionary(self):
		empty_transformer = WordsTransformator([])
		self.assertRaises(wtr.NoPathError, empty_transformer.findRoute, "aaaa", "bbbb")

	def test_node_existing(self):
		self.assertRaises(wtr.BadNameError, self.transformer.findRoute, "aaa", "cog")
		self.assertRaises(wtr.BadNameError, self.transformer.findRoute, "hit", "bbb")

# check that node is reachable
class TestNodeReachability(unittest.TestCase):
	# start with a good transformator
	def setUp(self):
		self.transformer = WordsTransformator(["hit", "dot", "dog", "cog", "hot", "log", "cat", "udo", "uso"])

	# single node with no connections (in fact not even inserted into the 'physical' graph)
	def test_noedges(self):
		self.assertRaises(wtr.DisconnectedError, self.transformer.findRoute, "cat", "cog")

	# no path, e.g. start and end words are in separate connected components
	def test_nopath(self):
		self.assertRaises(wtr.NoPathError, self.transformer.findRoute, "udo", "cog")

# cross check extreme input (edge cases)
class TestTransformationExtremes(unittest.TestCase):
	def setUp(self):
		self.initialised = True

	def permutations_helper(self, is_naive = False):
		# permutation length 1: (a, b, .., z) words set is dictionary itself
		words = permulationsWords(1)
		random.shuffle(words)
		wt = WordsTransformator(words, is_naive)

		num_transforms = wt.findNumTransformations('a', 'z')
		self.assertEqual(num_transforms, 1)
		num_transforms = wt.findNumTransformations('z', 'a')
		self.assertEqual(num_transforms, 1)

		# permutation length 2: (aa, ab, .., zz)
		words = permulationsWords(2)
		random.shuffle(words)
		wt = WordsTransformator(words, is_naive)
		num_transforms = wt.findNumTransformations('aa', 'zz')
		self.assertEqual(num_transforms, 2)
		num_transforms = wt.findNumTransformations('bb', 'by')
		self.assertEqual(num_transforms, 1)

		# permutation length 3
		words = permulationsWords(3)
		random.shuffle(words)
		wt = WordsTransformator(words, is_naive)
		num_transforms = wt.findNumTransformations('a'*3, 'z'*3)
		self.assertEqual(num_transforms, 3)

	def test_permutations(self):
		print "--optimised sliced-based transition matcher--"
		self.permutations_helper(False)
		#NOTE will take too long!!!
		#	print "--naive transition matcher--"
		#	self.permutations_helper(True)

	def test_largewords(self):
		#NOTE <basestring>.isalpha() does not work for strings too large!
		word_length = 1000*20

		# get a random word
		word = randomWord(word_length)
		assert(len(word) == word_length)

		# create words
		num_transforms_target = 10
		assert(num_transforms_target <= word_length)
		start = 0
		words = []
		# while start < num_transforms_target:
		# 	words.append(word)
		# 	word[]
		words.append(word)

		assert (rotateChar(word[0]).isalpha())
		assert (word[1:].isalpha())
		word = rotateChar(word[0]) + word[1:]
		assert(word.isalpha())
		words.append(word)

		word = word[:-1] + rotateChar(word[-1])
		assert(word.isalpha())
		words.append(word)

		wt = WordsTransformator(words)
		num_transforms = wt.findNumTransformations(words[0], words[-1])
		self.assertEqual(num_transforms, 2)

# test word of English dictionary
class TestTransformationRealDictionary(unittest.TestCase):
	def test_real_dictionary(self):
		# dictionary reader
		reader = ReadDictionary(en_dictionary_path)

		def testWithLength(length = 3):
			# get words
			words = reader.wordsWithLen(length)
			num_words = len(words)

			print "EN dictionary test, word length {0} of dictionary size {1}".format(length, num_words)

			# create transformator
			wt = WordsTransformator(words)

			# select two words on random
			word1 = random.choice(words)
			word2 = random.choice(words)
			# get different words!
			assert (num_words >= 2)
			while word1 == word2:
				#collision, recast dice!
				word2 = random.choice(words)

			# check that path transition is correct!
			path = wt.findRoute(word1, word2)
			print "checking path transition validity"
			self.assertTrue(nt.checkTransitions(path))

			num_transformations = wt.findNumTransformations(word1, word2)
			print '...between "{1}" and "{2}" there are {3} transformations.'\
				.format(length, word1, word2, num_transformations)

		try:
			testWithLength(3)
			testWithLength(3)
			testWithLength(4)
			testWithLength(5)
		except wtr.DisconnectedError:
			print "found a word with no connections at all!"
		except wtr.NoPathError:
			print "no transformation paths between two words found!"


# construct all permutations of words of the given length,
#	e.g. ["aa", "ab", "bb, .., "zz"] for length == 2
def permulationsWords(length = 1):
	assert(length >= 1)

	# initially words is a permutation list
	words = alphabet

	# incremet successively
	for i in range(length-1):
		new_words = []
		for w in words:
			for dw in alphabet:
				new_words.append(w + dw)
		# update words array
		words = new_words

	return words


# construct a word of random characters with a given length
def randomWord(length = 1):
	word = ""

	for i in range(length):
		rand_elem = random.choice(alphabet)
		word += rand_elem

	assert(word.isalpha())
	return word


# advance character value,
# 	e.g. a -> b, b -> c or z -> a
def rotateChar(c):
	c_input_ascii = ord(c)
	c_ascii = ((c_input_ascii + 1) % (ord('z') - ord('a') + 1)) + ord('a')
	c_out = chr(c_ascii)
	assert(c_out.isalpha())
	return c_out


if __name__ == '__main__':
	unittest.main()