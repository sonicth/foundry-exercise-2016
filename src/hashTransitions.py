import networkx as nx

# given groups relationships, construct a graph!
def _addGraphEdges(graph, words, neibor_groups):
	for group_idxs in neibor_groups:
		while len(group_idxs) > 1:
			idx1 = group_idxs.pop()
			word1 = words[idx1]

			for idx2 in group_idxs:
				word2 = words[idx2]

				#add edge!
				#NOTE labelling nodes with words here...
				#  it is also possible to label nodes with indices, but for simplicity just using words
				graph.add_edge(word1, word2)


# store their indices of the same prefixes
# (knows nothing about graph or the list of words)
def _groupNeiboursByWord(words, groups):

	# set of existing prefixes
	first_prefixes = {}

	# dictionary of 'adjacent' words, indexed by prefixes
	neibors = {}

	word_idx = 0
	for prefix in words:
		# two words with the same prefix?...
		if prefix in first_prefixes:
			# yes!
			if prefix in neibors:
				neibors[prefix].append(word_idx)
			else:
				# does not exist yet
				# first occurence of prefix
				first_occurence_idx = first_prefixes[prefix]
				# insert two occurences
				neibors[prefix] = [first_occurence_idx, word_idx]
		else:
			# does not exist (yet)
			first_prefixes[prefix] = word_idx

		word_idx += 1

	# construct graph from neighbours
	for group_prefix, group_idxs in neibors.items():
		# should be at least two neibours!
		assert(len(group_idxs) >= 2)

		groups.append(group_idxs)


# computer information needed to construct a graph,
#	in other words 'intermediate' relationships as lists of neighbours
#
# Algorithm description:
#
# a transition can exist at any character position
#	for instance in	[aabaa] (word index i)
#					[aacaa] (word index j)
#		there is a transition at the 3rd character (char index 2)
# each character position is rotated to the end, to see if there is a common beginning or 'prefix'
#	1. rotate	[aabaa] -> [aaaab]
#				[aacaa] -> [aaaac]
#	2. dicard last character, both strings become prefix [aaaa]
#	3. use dictionary to hash word indices to a prefix, storing collisions in a list
#		{ "aaaa": [i, j],
#			... }
#	4. list of two indices or more for a transition, or *edge*!
#		prefixes are no longer needed, just store all transitions together!
#	5. groups list becomes list of adjacency relationships [[i, j], [k, l], ...]
#
def _computeTransitions(words):
	groups = []
	word_length = len(words[0])

	# ...here go through each character position
	for char_pos in range(word_length):
		# ...rotate the word successively and removing the last character!
		discardLastChar_f = lambda w: rotate(w, char_pos)[:-1];
		prefixes = map(discardLastChar_f, words)

		# ...hash word indices to the same prefix
		#	and at the end at them to groups
		_groupNeiboursByWord(prefixes, groups)

	return groups


# rotate str(ing) by n
def rotate(str, n):
	return str[n:] + str[:n]


# created the graph for the given set of words
#	'sliced' method is used
def createGraphSliced(words):
	# compute transitions (or adjacencies of nodes) between words
	group_connections = _computeTransitions(words)

	# create graph
	graph = nx.Graph()

	# add graph edges from connections
	_addGraphEdges(graph, words, group_connections)

	return graph







