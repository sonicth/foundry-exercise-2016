import networkx as nx
import hashTransitions
import naiveTransitions

## Exceptions

# Base class for all Transformator exeptions used in this module
class TransformatorError(BaseException):
	pass


# wrong input parameter
class BadtypeError(TransformatorError):
	pass


# format of parameter incorrect, e.g. word lengths mismatch
class BadformatError(TransformatorError):
	pass


# word is not found in the dictionary
class BadNameError(TransformatorError):
	pass


# single node
class DisconnectedError(TransformatorError):
	pass

# no paths between two nodes
class NoPathError(TransformatorError):
	pass


# word correctness
def checkInstanceOneWord(word, first_word_length = -1):
	if not isinstance(word, basestring):
		print "***word is not a string!"
		raise BadtypeError

	if not word.isalpha():
		print "***word '{0}' contains non-alphabetic characters!".format(word)
		print word
		print word.isalpha()
		raise BadtypeError

	if not word.islower():
		print "***word contains upper-case characters!"
		raise BadtypeError

	# disable length check with '-1' value
	if first_word_length != -1:
		if len(word) != first_word_length:
			print "***words are of unequal length, found length {0} mismatches length {1} of the first word" \
				.format(len(word), first_word_length)
			raise BadformatError

	pass


# dictionary correctness
def checkInstanceWords(words):
	# must be list
	if not isinstance(words, list):
		print "***array expected!"
		raise TransformatorError

	# if any words...
	if len(words) > 0:
		# check first word and get the length
		checkInstanceOneWord(words[0])
		first_word_length = len(words[0])

		# words must be strings, lower-case, alphabetic, and have the same length
		map(lambda w: checkInstanceOneWord(w, first_word_length), words)


################################################################
# word transformator class
################################################################
class WordsTransformator:
	def __init__(self, dictionary, is_naive=False):
		# is word dictionary valid?
		checkInstanceWords(dictionary)

		self.dictionary = dictionary

		self.num_words = len(dictionary)
		if self.num_words > 0:
			self.word_length = len(dictionary[0])
			if is_naive:
				# high complexity w.r.t word count, low complexity for word length
				self.graph = naiveTransitions.createGraphNaive(dictionary)
			else:
				# low complexity w.r.t. word count, ok complexity for word length
				#NOTE see hashTransitions._computeTransitions() for algorithm description!
				self.graph = hashTransitions.createGraphSliced(dictionary)
		else:
			# any length is ok!
			self.word_length = 0

	def findRoute(self, start_word, end_word):
		# type: (word, word) -> array of words

		local_word_length = self.word_length
		# case for empty dictionaries
		if local_word_length == 0:
			local_word_length = len(start_word)

		# check query (node) words
		checkInstanceOneWord(start_word, local_word_length)
		checkInstanceOneWord(end_word, local_word_length)

		# empty path for empty dictionaries
		if self.word_length == 0:
			print "**empty dictionary, returning empty path"
			raise NoPathError

		# start and end must have connections
		self.checkNode(start_word)
		self.checkNode(end_word)

		try:
			# find minmal paths using Dijkstra Shortest Path algorithm
			path = nx.dijkstra_path(self.graph, start_word, end_word)
		except nx.NetworkXNoPath:
			print "***no path between {0} and {1}".format(start_word, end_word)
			raise NoPathError

		return path

	def checkNode(self, word):
		if not word in self.dictionary:
			print "***'{0}' word not in dictionary!".format(word)
			raise BadNameError
		else:
			nodes = self.graph.nodes()
			if not word in nodes:
				print "***'{0}' disconnected word!".format(word)
				raise DisconnectedError


	def findNumTransformations(self, start_word, end_word):
		path = self.findRoute(start_word, end_word)
		return len(path) - 1


